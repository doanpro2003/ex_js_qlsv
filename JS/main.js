var dssv = [];
const DSSV = "DSSV";
var dataJSON = localStorage.getItem(DSSV);
if (dataJSON) {
    var dataRaw = JSON.parse(dataJSON);

    dssv = dataRaw.map(function (sv) {
        return new SinhVien(
            sv.ma,
            sv.ten,
            sv.email,
            sv.matKhau,
            sv.diemLy,
            sv.diemToan,
            sv.diemHoa
        );
    });
    renderDssv(dssv);
}

function saveLocalStorage() {
    var dataJSON = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dataJSON);
}
function themSv() {
    var sv = layThongTinTuForm();
    dssv.push(sv);

    saveLocalStorage();
    renderDssv(dssv);

    // reset form
    document.getElementById("formQLSV").reset();
}

function xoaSv(idSv) {
    var index = dssv.findIndex(function (sv) {
        return sv.ma == idSv;
    });
    if (index == -1) return;
    dssv.splice(index, 1);
    saveLocalStorage();
    renderDssv(dssv);
}

function suaSv(idSv) {
    var index = dssv.findIndex(function (sv) {
        return sv.ma == idSv;
    });
    if (index == -1) return;
    var sv = dssv[index];
    hienThiThongTinTuForm(sv);
    document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
    var svEdit = layThongTinTuForm();
    var index = dssv.findIndex(function (sv) {
        return sv.ma == svEdit.ma;
    });
    if (index == -1) return;
    dssv[index] = svEdit;
    renderDssv(dssv);
    saveLocalStorage();
    document.getElementById("txtMaSV").disabled = false;
}

function resetForm() {
    document.getElementById("formQLSV").reset();
}
