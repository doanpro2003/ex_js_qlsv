function SinhVien(_ma, _ten, _email, _matKhau, _diemLy, _diemToan, _diemHoa) {
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.matKhau = _matKhau;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.tinhDTB = function () {
        if (this.diemToan < 0 || this.diemToan > 10 || this.diemHoa < 0 || this.diemHoa > 10 || this.diemLy < 0 || this.diemLy > 10) {
            alert('bạn đã nhập sai hãy sửa hoặc xoá và nhập lại điểm số');
            return;
        }
        else {
            var dtb = (this.diemToan + this.diemLy + this.diemHoa) / 3;
            return dtb.toFixed(1);
        }
    };
}
